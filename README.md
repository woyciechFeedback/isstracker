ISSTracker is a simple app intended for learning RxJava, Room and Retrofit as well as Model-View-Intent architecture.
It is tracking and showing actual position of _International Space Station_.

It is currently under development.

The app is to comply with following specs:

* ISS position and current crew members should be obtained from http://api.open-notify.org/iss-now.json and http://api.open-notify.org/iss-now.json.

* For displaying the map Mapbox library should be used.

* Once the application is started it should move the map camera to the last known position of the ISS, if possible.
* Clicking on the map marker representing the ISS should display a map bubble annotation with a list of people currently in space (this list can be a simple string).
* The last status of the ISS (position and time) should be stored locally on the device (in a database or in shared preferences) and updated every time we get data from the API.
* Along with the map view, the app should display the time of last API sync somewhere in the UI.
* The application should query the API for data periodically with some fixed time interval (e.g. 5 seconds) and reflect the new state in the UI.
