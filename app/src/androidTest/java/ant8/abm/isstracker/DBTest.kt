package ant8.abm.isstracker

import android.arch.persistence.room.EmptyResultSetException
import android.support.test.runner.AndroidJUnit4
import io.reactivex.Single
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DBTest : AnkoLogger, DBTestBase() {

    @Test
    fun shouldSelectPositionAfterPositionInserted() {
        Single
                .fromCallable { dao.insert(EXEMPLARY_POSITION) }
                .flatMap {
                    dao.getPosition()
                }
                .test()
                .assertResult(EXEMPLARY_POSITION)
    }

    @Test
    fun shouldSelectPositionAfterPositionInsertedSynchronous() {
        dao.insert(EXEMPLARY_POSITION)
        dao.getPosition()
                .test()
                .assertResult(EXEMPLARY_POSITION)
    }

    @Test
    fun shouldReturnNoValuesWhenDbEmpty() {
        dao.getPosition()
            .doOnError { info("on error no values $it") }
            .test()
            .assertNoValues()
            .assertError(EmptyResultSetException::class.java)
    }
}
