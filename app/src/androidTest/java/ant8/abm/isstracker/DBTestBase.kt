package ant8.abm.isstracker

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import ant8.abm.isstracker.repo.IssPositionData
import ant8.abm.isstracker.repo.IssPositionDao
import ant8.abm.isstracker.repo.IssPositionDb
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.junit.After
import org.junit.Before
import org.junit.Rule

open class DBTestBase : AnkoLogger {
    private lateinit var db: IssPositionDb
    protected lateinit var dao: IssPositionDao

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getTargetContext(), IssPositionDb::class.java)
                .allowMainThreadQueries()
                .build()
        dao = db.issPositionDao()
        info("db open?, ${db.isOpen}")
    }

    @After
    fun closeDb() {
        db.close()
        info("db closed?, ${!db.isOpen}")
    }
}