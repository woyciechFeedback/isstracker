package ant8.abm.isstracker

import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import ant8.abm.isstracker.reducer.IssReducer
import ant8.abm.isstracker.repo.IssCrew
import ant8.abm.isstracker.repo.IssPositionData
import io.reactivex.subjects.SingleSubject
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    val positionApiMock = SingleSubject.create<IssPositionData>()
    val crewApiMock = SingleSubject.create<IssCrew>()

    @JvmField
    @Rule
    val rule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun afterActivityLaunched() {
            activity.reducer = IssReducer({ positionApiMock }, { crewApiMock })
            super.afterActivityLaunched()
        }
    }

    @Test
    fun shouldStartActivity() {}

    @Test
    fun shouldHaveMapVisible() {
        Espresso.onView(withId(R.id.mapView))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

}

