package ant8.abm.isstracker

import android.support.test.runner.AndroidJUnit4
import ant8.abm.isstracker.repo.NetworkApi
import io.appflate.restmock.RESTMockServer
import io.appflate.restmock.utils.RequestMatchers.pathContains
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

@RunWith(AndroidJUnit4::class)
class NetworkApiTest : AnkoLogger {
    lateinit var url: String
    val retrofit by lazy { Retrofit.Builder()
        .baseUrl(url)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(JacksonConverterFactory.create())
        .build()
    }
    val api by lazy { retrofit.create(NetworkApi::class.java) }
    val BASE_URL = "http://api.open-notify.org"
    val EXEMPLARY_CREW_LIST = listOf(
        "Oleg Artemyev",
        "Andrew Feustel",
        "Richard Arnold",
        "Sergey Prokopyev",
        "Alexander Gerst",
        "Serena Aunon-Chancellor")

    @Test
    fun positionApiCallsNetwork() {
        url = BASE_URL
        api
            .getPosition()
            .subscribe( { info("${it.message}, ${it.latitude}, ${it.timestamp}") }, { error(it) })
    }

    @Test
    fun crewApiCallsNetwork() {
        url = BASE_URL
        api
            .getCrew()
            .subscribe( { info("${it.people}") }, { error(it) })
    }

    @Test
    fun shouldPositionApiReturnsValueWhenProvided() {
        url = RESTMockServer.getUrl()
        RESTMockServer
            .whenGET(pathContains("iss-now.json"))
            .thenReturnFile(200, "position.json")

        api
            .getPosition()
            .test()
            .assertValue(EXEMPLARY_POSITION)
    }

    @Test
    fun shouldPositionApiReturnErrorWhenServerReturnsErrorenousData() {
        url = RESTMockServer.getUrl()
        RESTMockServer
            .whenGET(pathContains("iss-now.json"))
            .thenReturnFile(200, "position_w_error.json")

        api
            .getPosition()
            .doOnError { info("error for data $it") }
            .test()
            .assertError(Exception::class.java)

    }

    @Test
    fun shouldCrewApiReturnCrewWhenServerProvided() {
        url = RESTMockServer.getUrl()
        RESTMockServer
            .whenGET(pathContains("astros.json"))
            .thenReturnFile(200, "crew.json")

        api
            .getCrew()
            .test()
            .assertValue{ it.people == EXEMPLARY_CREW_LIST }

    }

    @Test
    fun shouldCrewApiReturnErrorWhenServerReturnsErrorenousData() {
        url = RESTMockServer.getUrl()
        RESTMockServer
            .whenGET(pathContains("astros.json"))
            .thenReturnFile(200, "crew_w_error.json")

        api
            .getCrew()
            .doOnError { info("error for data $it") }
            .test()
            .assertError(Exception::class.java)

    }
}
