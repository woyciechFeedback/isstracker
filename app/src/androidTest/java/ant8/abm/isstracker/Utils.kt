package ant8.abm.isstracker

import ant8.abm.isstracker.repo.IssPositionData

val EXEMPLARY_POSITION = IssPositionData(message = "success", latitude = -7.1328, longitude = -37.1462, timestamp = 1534072516)
val EXEMPLARY_POSITION_2 = IssPositionData(1, "success", -8.0584, 118.8641, 1528820791)
