package ant8.abm.isstracker

import android.support.multidex.MultiDexApplication
import com.mapbox.mapboxsdk.Mapbox

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        Mapbox.getInstance(applicationContext, getString(R.string.mapbox_access_token))
    }
}