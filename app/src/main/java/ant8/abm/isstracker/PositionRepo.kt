package ant8.abm.isstracker

import ant8.abm.isstracker.repo.IssPositionData
import ant8.abm.isstracker.repo.IssPositionDao
import ant8.abm.isstracker.repo.NetworkApi
import io.reactivex.Single

class PositionRepo(val api: NetworkApi, val dao: IssPositionDao) {

    fun getPosition(): Single<IssPositionData> =
        dao.getPosition()
            .onErrorResumeNext { api.getPosition() }
            .doOnSuccess { dao.insert(it) }
}