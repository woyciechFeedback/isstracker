package ant8.abm.isstracker.elpassionhelpers

//as per El Passion workshops

import io.reactivex.Observable

typealias Events = Observable<Any>

typealias Reducer<T> = (events: Events) -> Observable<T>