package ant8.abm.isstracker.reducer

import ant8.abm.isstracker.repo.IssCrew
import ant8.abm.isstracker.repo.IssPositionData
import io.reactivex.Single

interface Iss {
    data class State(val position: Position.State, val crewState: Crew.State)

    interface Position {
        object FetchPositionEvent

        sealed class State {
            object Idle : State()
            object Loading : State()
            data class PositionKnown(val position: ant8.abm.isstracker.repo.IssPositionData) : State()
            object PositionUnknown : State()
        }
    }

    interface Crew {
        object FetchCrew

        sealed class State {
            object Idle : State()
            object Loading : State()
            object Unknown : State()
            data class Known(val cosmonauts: List<String>) : State()
        }
    }
}