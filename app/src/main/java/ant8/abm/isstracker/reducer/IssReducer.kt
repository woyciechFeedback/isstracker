package ant8.abm.isstracker.reducer

import ant8.abm.isstracker.elpassionhelpers.Events
import ant8.abm.isstracker.elpassionhelpers.Reducer
import ant8.abm.isstracker.repo.IssCrew
import ant8.abm.isstracker.repo.IssPositionData
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.Observables

class IssReducer(val positionApi: () -> Single<IssPositionData>, val crewApi: () -> Single<IssCrew>) : Reducer<Iss.State> {
    override fun invoke(events: Events): Observable<Iss.State> =
            Observables.combineLatest(positionEventsHandler(events), crewEventsHandler(events), Iss::State)

    fun positionEventsHandler(events: Events): Observable<Iss.Position.State> = events
            .ofType(Iss.Position.FetchPositionEvent::class.java)
            // leaving these because I'm checking response to the apis in isolation
            .switchMap { positionApi.invoke()
                    .toObservable()
                    .map<Iss.Position.State> { Iss.Position.State.PositionKnown(it) }
                    .onErrorReturn { Iss.Position.State.PositionUnknown }
                    .startWith(Iss.Position.State.Loading) }
            .startWith(Iss.Position.State.Idle)

    fun crewEventsHandler(events: Events): Observable<Iss.Crew.State> = events
            .ofType(Iss.Crew.FetchCrew::class.java)
            .switchMap { crewApi.invoke()
                    .toObservable()
                    .map { it.people }
                    .map<Iss.Crew.State> { Iss.Crew.State.Known(emptyList()) }
                    .onErrorReturn { Iss.Crew.State.Unknown }
                    .startWith(Iss.Crew.State.Loading)
            }
            .startWith(Iss.Crew.State.Idle)
}