package ant8.abm.isstracker.repo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.mapbox.mapboxsdk.geometry.LatLng

@Entity(tableName = "iss_position")
data class IssPositionData(@PrimaryKey(autoGenerate = false) var id: Int = 1,
                       var message: String = "",
                       var latitude: Double = 0.0,
                       var longitude: Double = 0.0,
                       var timestamp: Long = 0L) {

    @JsonCreator
    constructor(@JsonProperty("message") message: String, @JsonProperty("iss_position") position: Map<String, Double>,
                @JsonProperty("timestamp") timestamp: Long)
            : this(1, message, position["latitude"]!!, position["longitude"]!!, timestamp)

    fun toLatLng(): LatLng = LatLng(this.latitude, this.longitude)
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class IssCrew(
        @JsonProperty("people")
        private val _people: List<Map<String, String>>) {
    val people = _people.map { it["name"] }
}