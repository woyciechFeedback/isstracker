package ant8.abm.isstracker.repo

import android.arch.persistence.room.*
import io.reactivex.Single

@Database(entities = [IssPositionData::class], version = 1)
abstract class IssPositionDb : RoomDatabase() {
    abstract fun issPositionDao(): IssPositionDao
}

@Dao
interface IssPositionDao {
    @Query("SELECT * FROM $TABLE_NAME LIMIT 1")
    fun getPosition(): Single<IssPositionData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(position: IssPositionData)

    companion object {
        const val TABLE_NAME = "iss_position"
    }

}