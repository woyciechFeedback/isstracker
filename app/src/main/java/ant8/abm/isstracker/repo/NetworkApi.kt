package ant8.abm.isstracker.repo

import io.reactivex.Single
import retrofit2.http.GET

interface NetworkApi {
    @GET("iss-now.json")
    fun getPosition(): Single<IssPositionData>

    @GET("astros.json")
    fun getCrew(): Single<IssCrew>
}