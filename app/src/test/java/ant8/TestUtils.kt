package ant8

//as per El Passion workshops

import io.reactivex.observers.TestObserver

fun <T> TestObserver<T>.assertLastValue(expected: T) {
    junit.framework.Assert.assertEquals(expected, values().last())
}

fun <T> TestObserver<T>.assertLastValueThat(predicate: T.() -> Boolean) {
    junit.framework.Assert.assertTrue("Last value does not match predicate. ${values().last()}", values().last().predicate())
}