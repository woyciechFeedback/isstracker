package ant8.abm.isstracker

import ant8.abm.isstracker.reducer.IssReducer
import ant8.abm.isstracker.reducer.Iss
import ant8.abm.isstracker.repo.IssCrew
import ant8.abm.isstracker.repo.IssPositionData
import ant8.assertLastValueThat
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.SingleSubject
import org.junit.Test

class IssReducerTest {

    val events = PublishRelay.create<Any>()
    val positionApi = SingleSubject.create<IssPositionData>()
    val crewApi = SingleSubject.create<IssCrew>()
    val state: TestObserver<Iss.State> = IssReducer({ positionApi }, { crewApi }).invoke(events).test()

    val exemplaryPosition = IssPositionData(1, "success", -7.0584, 108.8641, 1528813791)

    @Test
    fun shouldIssPositionDataBeIdleOnStart() {
        state.assertLastValueThat { position == Iss.Position.State.Idle }
    }

    @Test
    fun shouldIssPositionDataBeLoadingAfterFetchPositionEvent() {
        events.accept(Iss.Position.FetchPositionEvent)
        state.assertLastValueThat { position == Iss.Position.State.Loading }
    }

    @Test
    fun shouldIssPositionDataBeKnownWhenPositionApiReturnsOk() {
        events.accept(Iss.Position.FetchPositionEvent)
        positionApi.onSuccess(exemplaryPosition)
        state.assertLastValueThat { position == Iss.Position.State.PositionKnown(exemplaryPosition) }
    }

    @Test
    fun shouldIssPositionDataBeUnknownWhenPositionApiReturnsError() {
        events.accept(Iss.Position.FetchPositionEvent)
        positionApi.onError(Exception("Error"))
        state.assertLastValueThat { position == Iss.Position.State.PositionUnknown }
    }
    
    @Test
    fun shouldIssCrewStateBeIdleAtStart() {
        state.assertLastValueThat { crewState == Iss.Crew.State.Idle }
    }

    @Test
    fun shouldIssCrewBeLoadingWhenRequested() {
        events.accept(Iss.Crew.FetchCrew)
        state.assertLastValueThat { crewState == Iss.Crew.State.Loading }
    }

    @Test
    fun shouldIssCrewBeKnownWhenApiReturnsOk() {
        events.accept(Iss.Crew.FetchCrew)
        crewApi.onSuccess(IssCrew(emptyList()))
        state.assertLastValueThat { crewState == Iss.Crew.State.Known(emptyList()) }
    }

    @Test
    fun shouldIssCrewBeUnknownWhenApiReturnsError() {
        events.accept(Iss.Crew.FetchCrew)
        crewApi.onError(Exception("Error"))
        state.assertLastValueThat { crewState == Iss.Crew.State.Unknown }
    }

}