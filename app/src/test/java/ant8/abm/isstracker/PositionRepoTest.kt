package ant8.abm.isstracker

import android.arch.persistence.room.EmptyResultSetException
import ant8.abm.isstracker.repo.IssPositionData
import ant8.abm.isstracker.repo.IssPositionDao
import ant8.abm.isstracker.repo.NetworkApi
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import java.util.concurrent.TimeUnit

class PositionRepoTest {
    val api by lazy { mock<NetworkApi>() }
    val dao by lazy { mock<IssPositionDao>() }
    val repo by lazy { PositionRepo(api, dao) }
    val EXEMPLARY_POSITION = IssPositionData(1, "success", -7.0584, 108.8641, 1528813791)
    val EXEMPLARY_POSITION_2 = IssPositionData(1, "success", -8.0584, 118.8641, 1528820791)
    val TEST_SCHEDULER = TestScheduler()

    @Before
    fun setup() {
        RxJavaPlugins.setComputationSchedulerHandler { TEST_SCHEDULER }
    }

    @After
    fun cleanup() {
        RxJavaPlugins.setComputationSchedulerHandler(null)
    }

    @Test
    fun shouldReturnPositionWhenApiReturnsForFirstTime() {
        whenever(api.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION))
        whenever(dao.getPosition()).thenReturn(Single.error(EmptyResultSetException("No results")))
        repo.getPosition()
            .test()
            .assertValue(EXEMPLARY_POSITION)
    }

    @Test
    fun shouldReturnEmptyWhenApiReturnsEmptyForFirstTime() {
        whenever(api.getPosition()).thenReturn(Single.error(Exception("No results")))
        whenever(dao.getPosition()).thenReturn(Single.error(EmptyResultSetException("No results")))
        repo.getPosition()
            .test()
            .assertNoValues()
            .assertError(Exception::class.java)
    }

    @Test
    fun shouldCallDbInsertOnCorrectResponse() {
        whenever(api.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION))
        whenever(dao.getPosition()).thenReturn(Single.error(Exception("No results")))
        repo.getPosition()
            .test()

        verify(dao).insert(EXEMPLARY_POSITION)
    }

    @Test
    fun shouldReturnLastDataFromDbWhenNetworkUnavailable() {
        whenever(api.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION))
        whenever(dao.getPosition()).thenReturn(Single.error(Exception("No results")))
        repo.getPosition()
            .test()

        whenever(api.getPosition()).thenReturn(Single.error(Exception("No results")))
        whenever(dao.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION))

        repo.getPosition()
            .test()
            .assertValue(EXEMPLARY_POSITION)

        TEST_SCHEDULER.advanceTimeBy(2, TimeUnit.SECONDS)
    }

    @Test
    fun shouldReturnErrorWhenBothDbAndApiReturnError() {
        whenever(api.getPosition()).thenReturn(Single.error(Exception("No results")))
        whenever(dao.getPosition()).thenReturn(Single.error(Exception("No results")))

        repo.getPosition()
            .test()
            .assertNoValues()
            .assertError(Exception::class.java)

    }

    @Test
    fun shouldReturnFromCacheWhenApiLingers() {
        whenever(api.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION_2)
            .delay(1, TimeUnit.SECONDS))
        whenever(dao.getPosition()).thenReturn(Single.just(EXEMPLARY_POSITION))

        repo.getPosition()
            .test()
            .assertValue(EXEMPLARY_POSITION)

        TEST_SCHEDULER.advanceTimeBy(2, TimeUnit.SECONDS)
    }

    @Test
    fun shouldNotCallInsertWhenNoDataFromApiForFirstTime() {
        whenever(api.getPosition()).thenReturn(Single.error(Exception("No response")))
        whenever(dao.getPosition()).thenReturn(Single.error(EmptyResultSetException("No data")))

        repo.getPosition()
            .test()

        verify(dao, never()).insert(anyPositionMock())
    }

    private fun anyPositionMock() = any(IssPositionData::class.java) ?: EXEMPLARY_POSITION

}